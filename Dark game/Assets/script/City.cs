using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class City : MonoBehaviour
{
    public List<Charapter> charapters;
    // Start is called before the first frame update
    void Start()
    {
        charapters.Add(newVillager());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Charapter newVillager()
    {
        return new Charapter(UnityEngine.Random.Range(0, 16));
    }
}
